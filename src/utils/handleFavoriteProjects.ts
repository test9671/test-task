import { Project } from '../components/ProjectList';

export const setFavoriteProjects = (project: Project) => {
  const favorites = localStorage.getItem('favorites');
  if (favorites) {
    const favArr: Project[] = JSON.parse(favorites);
    const isExist = favArr.some((el) => el.id === project.id);
    if (!isExist) {
      favArr.push(project);
      localStorage.setItem('favorites', JSON.stringify(favArr));
    }
  } else {
    localStorage.setItem('favorites', JSON.stringify([project]));
  }
};

export const getFavoriteProjects = (): Project[] => {
  const favorites = localStorage.getItem('favorites');
  return favorites ? JSON.parse(favorites) : [];
};
