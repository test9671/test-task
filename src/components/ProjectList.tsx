import { useCallback, useEffect, useRef, useState } from 'react';
import { API_URL } from '../api/constants';
import Card from './Card';

export type Project = {
  id: number | string;
  name: string;
  description: string;
  language: string;
  stargazers_count: number;
  owner: {
    avatar_url: string;
  };
  html_url: string;
};

const ProjectList = ({ search }: { search: string }) => {
  const [selectedLang, setSelectedLang] = useState('');
  const [projects, setProjects] = useState<Project[]>([]);
  const optionsRef = useRef<string[]>();

  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedLang(event.target.value);
  };

  const createOptions = (items: Project[]) => {
    const languageList = items.map((el) => el.language).filter(Boolean);
    optionsRef.current = Array.from(new Set(languageList));
  };

  const fetchProjects = useCallback(async (query: string) => {
    try {
      const response = await fetch(`${API_URL}?q=${query}`);
      const { items } = (await response.json()) as { items: Project[] };

      if (items.length && !optionsRef.current) {
        createOptions(items);
      }
      setProjects(items);
    } catch (err) {
      console.error(err);
    }
  }, []);

  useEffect(() => {
    if (search || selectedLang) {
      const query = `${search} in:name language:${selectedLang}`;
      fetchProjects(encodeURIComponent(query));
    }
  }, [fetchProjects, search, selectedLang]);

  return (
    <div>
      {optionsRef.current && (
        <select
          name='langs'
          id='lang-select'
          value={selectedLang}
          onChange={handleChange}
        >
          <option disabled value=''>
            Select Language
          </option>
          {optionsRef.current.map((option, i) => (
            <option key={i} value={option}>
              {option}
            </option>
          ))}
        </select>
      )}
      {projects.map((el) => (
        <Card key={el.id} {...el} />
      ))}
    </div>
  );
};

export default ProjectList;
