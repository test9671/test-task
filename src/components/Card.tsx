import React from 'react';
import { setFavoriteProjects } from '../utils/handleFavoriteProjects';
import { Project } from './ProjectList';

const Card = (project: Project) => {
  return (
    <div>
      <hr />
      <img src={project.owner.avatar_url} alt='avatar' width={100} />
      <div style={{ fontWeight: 'bold' }}>
        Project name:{' '}
        <a href={project.html_url} target='_blank' rel='noreferrer'>
          {project.name}
        </a>
      </div>
      <div style={{ fontSize: 14 }}>Stars: {project.stargazers_count}</div>
      <span style={{ fontSize: 12 }}>{project.description}</span>
      <p style={{ fontSize: 14 }}>Language: {project.language}</p>
      <button onClick={() => setFavoriteProjects(project)}>
        Add to favorites
      </button>
    </div>
  );
};

export default Card;
