import { getFavoriteProjects } from '../utils/handleFavoriteProjects';

function FavoriteProjects() {
  const favoriteProjects = getFavoriteProjects();
  return (
    <ol>
      {favoriteProjects.map(({ id, name }) => (
        <li key={id}>{name}</li>
      ))}
    </ol>
  );
}

export default FavoriteProjects;
