import React from 'react';
import { Link } from 'react-router-dom';

function Menu() {
  return (
    <ul>
      <li>
        <Link to='/favorite'>Favorite Projects</Link>
      </li>
      <li>
        <Link to='/main'>Main</Link>
      </li>
    </ul>
  );
}

export default Menu;
