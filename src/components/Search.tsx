import React, { useRef } from 'react';

function Search({
  setSearch,
}: {
  setSearch: React.Dispatch<React.SetStateAction<string>>;
}) {
  const ref = useRef<HTMLInputElement>(null);
  const handleSearch = () => {
    if (ref.current) {
      setSearch(ref.current.value);
    }
  };
  return (
    <div>
      <input type='text' ref={ref} />
      <button onClick={handleSearch}>Search</button>
    </div>
  );
}

export default Search;
