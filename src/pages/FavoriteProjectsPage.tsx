import React from 'react';
import FavoriteProjects from '../components/FavoriteProjects';

function FavoriteProjectsPage() {
  return (
    <div>
      <h1>My Favorite Projects</h1>
      <FavoriteProjects />
    </div>
  );
}

export default FavoriteProjectsPage;
