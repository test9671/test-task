import React, { useState } from 'react';
import ProjectList from '../components/ProjectList';
import Search from '../components/Search';

function Main() {
  const [search, setSearch] = useState('');

  return (
    <div>
      <h4>Search projects</h4>
      <Search setSearch={setSearch} />
      <ProjectList search={search} />
    </div>
  );
}

export default Main;
