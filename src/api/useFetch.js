import { useEffect, useState } from 'react';

import { API_URL } from './constants';

const useFetch = (search) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch(`${API_URL}?q=${search}&page=1`)
      .then((response) => response.json())
      .then((json) => setData(json.items))
      .catch(console.error);
  }, [search]);

  return { data };
};

export default useFetch;
