import { Route, Routes } from 'react-router-dom';
import './App.css';
import Menu from './components/Menu';
import FavoriteProjectsPage from './pages/FavoriteProjectsPage';
import Main from './pages/Main';

function App() {
  return (
    <>
      <Menu />
      <Routes>
        <Route path='/favorite' element={<FavoriteProjectsPage />} />
        <Route path='/main' element={<Main />} />
      </Routes>
    </>
  );
}

export default App;
